import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;

import java.util.List;

public class Desafio_Correto {

    private WebDriver driver;

    @Before
    public void abrirNavegador() {
        System.setProperty("webdriver.gecko.driver", "C:/Windows/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.magazineluiza.com.br");
    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void DesafioFabrica() throws InterruptedException {

        driver.findElement(By.xpath("/html/body/div[1]/div/main/div/div[2]/button")).click();
        Thread.sleep(2000);
        //digitar o nome do produto
        driver.findElement(By.xpath("//*[@id=\"input-search\"]")).sendKeys("Playstation 5"+Keys.ENTER);

        //validar o resultado da pesquisa
        Thread.sleep(4000);
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[1]/div/main/section[4]/div[1]")).getText().contains("Resultados"));

        //clicar no objeto indicado
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/div[1]/div/main/section[4]/div[3]/div/ul/li[3]/a")).click();

        //Adiconar no carrinho
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/div[1]/div/main/section[4]/div[6]/div/button")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/div[1]/div/main/section[2]/div[2]/div/div[2]/div/div[1]/div[2]/button")).click();

        //validar o produto no carrinho
        Thread.sleep(2000);
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]")).getText().contains("Sacola"));

    }
}